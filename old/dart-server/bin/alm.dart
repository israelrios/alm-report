library alm;

import 'dart:io';
import 'dart:async';
import 'dart:convert';
import 'package:xml/xml.dart' as xml;
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';

import 'date.dart';

final Duration TIMEOUT = new Duration(seconds: 30);

Logger logger = new Logger("alm");

String encodeParams(Map<String, String> params) {
  var encoded = [];
  params.forEach((name, val) => encoded.add(name + "=" + Uri.encodeQueryComponent(val)));
  return encoded.join("&");
}

class LoginException implements Exception {
}

class AlmException implements Exception {
  final String message;

  const AlmException(this.message);

  String toString() {
    if (message == null) return "AlmException";
    return "AlmException: $message";
  }
}

class TimeSheetEntry {
  String username;
  Date date;
  int spent;

  TimeSheetEntry(this.username, this.date, this.spent);

  static Date parseDate(String text) {
    var parts = text.substring(0, 10).split('-');
    return new Date(int.parse(parts[0]), int.parse(parts[1]), int.parse(parts[2]));
  }
}

class WorkItem {
  String id;
  String name;
  String url;
  List<TimeSheetEntry> timesheet = [];

  WorkItem(this.id, [this.name, this.url]) {
  }

  int daytotal(Date date, String username) {
    var sum = 0;
    for (var tse in timesheet) {
      if (tse.username == username && tse.date == date) {
        sum += tse.spent;
      }
    }
    return sum;
  }
}

class DaySummary {
  int sum = 0;
  List<WorkItem> workitems = [];
}

HttpClient makeHttpClient() {
  return new HttpClient();
}

class AlmReport {

  static const String ccm = "https://alm.serpro/ccm/";
  static const String urlLogin = '${ccm}auth/j_security_check';
  static const String urlAuthFailed = "${ccm}auth/authfailed";
  static const String urlLogout = '${ccm}service/com.ibm.team.repository.service.internal.ILogoutRestService';
  static const String urlRptWorkItem = '${ccm}rpt/repository/workitem?size=200&';
  static const String urlShowWorkItem = "${ccm}resource/itemName/com.ibm.team.workitem.WorkItem/";

  // 2014-08-18T10:00:00.000-0000 timezone é adicionado depois
  static final DateFormat RPT_DATE_FORMAT = new DateFormat("y'-'MM'-'dd'T'HH':'mm':'ss'.'SSS");

  //TODO: Exibir tarefas em progresso.
  // estados do grupo "closed":
  // ${ccm}rpt/repository/workitem?fields=workItem/projectArea/states[group=closed]/id&size=1
  // ${ccm}rpt/repository/workitem?fields=workitem/workItem[owner/userId=${username} and state/id!=estados_grupo_closed]/(id|summary)

  static final HttpClient client = makeHttpClient();

  final String username;
  final String password;
  Set<Cookie> cookies = new Set();

  AlmReport(this.username, this.password) {
  }

  Function setHeaders([Map<String, String> params = null]) {
    Future<HttpClientResponse> onrequest(HttpClientRequest request) {
      request.cookies.addAll(cookies);
      request.followRedirects = true;
      if (params != null) {
        request.headers.contentType = new ContentType("application", "x-www-form-urlencoded", charset: "utf-8");
        request.write(encodeParams(params));
      }
      return request.close();
    }
    return onrequest;
  }

  FutureOr<String> handleResponse(HttpClientResponse response) {
    if (response.statusCode >= 400) {
      throw new AlmException(response.reasonPhrase);
    }
    var location = response.headers.value("location");
    if (location != null && location.startsWith(urlAuthFailed)) {
      throw new LoginException();
    }
    try {
      cookies.addAll(response.cookies);
    } on FormatException {
      // ignora porque o logout sempre vem um cookie com formato inválido
    }

    //cookies.forEach((cookie) => print(cookie.name));
    return response.transform(utf8.decoder).join();
  }

  Future<String> post(String url, [Map<String, String> params = null]) {
    return client.postUrl(Uri.parse(url)).then(setHeaders(params)).timeout(TIMEOUT).then(handleResponse);
  }

  Future<String> get(String url) {
    return client.getUrl(Uri.parse(url)).then(setHeaders()).timeout(TIMEOUT).then(handleResponse);
  }

  Future<AlmReport> login() {
    var params = {
      'j_username': username,
      'j_password': password
    };
    return post(urlLogin, params).then((String data) {
      return this;
    });
  }

  Iterable<WorkItem> extractWorkItems(String xmltext) {
    var doc = xml.parse(xmltext);
    Map<String, WorkItem> workitems = {};
    for (var winode in doc.findAllElements("workItem")) {
      if (winode.children.isEmpty) {
        // quando um timesheet é excluído do workitem ele ainda aparece na consulta, porém sem workitem.
        continue;
      }
      var wid = winode.findElements("id").first.text;
      var witem = workitems.putIfAbsent(wid, () => new WorkItem(wid, winode.findElements("summary").first.text.trim(), urlShowWorkItem + wid));
      var tsnode = winode.parent as xml.XmlParent;
      var timeSpent = tsnode.findElements("timeSpent").first.text;
      var startDate = tsnode.findElements("startDate").first.text;
      var tse = new TimeSheetEntry(username, TimeSheetEntry.parseDate(startDate), int.parse(timeSpent));
      witem.timesheet.add(tse);
    }
    return workitems.values;
  }

  Future<Iterable<WorkItem>> loadWorkItems(Date date, int daysback) {
    logger.info('Loading workitems - ${username}');
    var mindate = date.subtract(new Duration(days: daysback)).toLocal();
    var startDate = formatQueryDate(mindate);
    String maxDateExpression = "";
    if (date != new Date.now()) {
      String endDate = formatQueryDate(date.add(new Duration(days: 1)).toLocal());
      maxDateExpression = " and startDate<" + endDate;
    }
    // service doc:
    // http://open-services.net/pub/Main/ReportingHome/Reportable_Rest_Services_Interfaces-OSLC_Submission.pdf
    // https://jazz.net/wiki/bin/view/Main/ReportsRESTAPI#time_SheetEntry_type_com_ibm_tea
    var expression = "workitem/timeSheetEntry[creator/userId=${username} and startDate>${startDate}${maxDateExpression}]/(timeSpent|startDate|creator/userId|workItem/(id|summary))";
    var url = urlRptWorkItem + encodeParams({
      'fields': expression
    });
    return get(url).then(extractWorkItems);
  }

  String formatQueryDate(DateTime date) {
    var tz = date.timeZoneOffset;
    var timezone = tz.isNegative ? '-' : '+';
    timezone += tz.inHours.abs().toString().padLeft(2, '0') + (tz.inMinutes.abs() % 60).toString().padLeft(2, '0');
    return RPT_DATE_FORMAT.format(date) + timezone;
  }

  Map<Date, DaySummary> buildDayMap(Iterable<WorkItem> workitems, Date date, int daysback) {
    Map<Date, DaySummary> daymap = {};
    for (WorkItem wi in workitems) {
      for (var tse in wi.timesheet) {
        // considera somente os últimos daysback dias
        if (date.difference(tse.date).inDays > daysback || tse.username != username || tse.spent == 0) {
          continue;
        }
        var day = daymap.putIfAbsent(tse.date, () => new DaySummary());
        day.sum += tse.spent;
        day.workitems.add(wi);
      }
    }
    return daymap;
  }

  Future<Map<Date, DaySummary>> build(Date date, int daysback) {
    return loadWorkItems(date, daysback).then((workitems) => buildDayMap(workitems, date, daysback));
  }

  Future<AlmReport> logout() {
    return post(urlLogout).then((text) {
      cookies.clear();
      logger.info('Logged out - ${username}');
      return this;
    });
  }
}

void main(List<String> args) {
  var username = "";
  var password = "";
  AlmReport ar = new AlmReport(username, password);
  ar.login().then((service) {
    var today = new Date.now();
    int daysback = 7 + today.weekday;
    if (daysback < 14) {
      daysback += 7;
    }
    return service.build(today, daysback).then((daymap) {
      for (var date in daymap.keys) {
        print("${date}: ${daymap[date].workitems[0].name}");
      }
    });
  }).whenComplete(() => ar.logout());

}
