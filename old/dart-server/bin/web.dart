import 'dart:async';
import 'dart:io';

import 'package:aqueduct/aqueduct.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';

import 'alm.dart';
import 'config.dart';
import 'date.dart';

const LOGIN_ERROR = "Não foi possível fazer login no Alm.";

Logger logger = new Logger("web");

final DateFormat DATE_FORMAT = new DateFormat("E d/MMM");

Set<Date> holydays = new Set();

Map response(data) {
  return {'data': data};
}

Map error(msg) {
  return response({"error": msg});
}

String formatMillis(int millis) {
  if (millis == 0) {
    return "0";
  }
  int minutes = (millis / 1000 / 60).truncate();
  return "${(minutes / 60).truncate()}".padLeft(2, '0') + ":" + "${(minutes % 60)}".padLeft(2, '0');
}

Object buildReportData(Map<Date, DaySummary> daymap, Date baseDate, int daysback, String username) {
  // monta o relatório
  var oneday = new Duration(days: 1);

  var results = [];

  var end = baseDate;
  daymap.keys.forEach((val) => end = val.isAfter(end) ? val : end);

  var day = baseDate.subtract(oneday * daysback);
  while (!day.isAfter(end)) {
    // gera o relatório do dia
    DaySummary daySummary = daymap[day];
    var total = 0, workitems = [];

    if (daySummary != null) {
      for (var wi in daySummary.workitems) {
        workitems.add(
            {'id': wi.id, 'name': wi.name, 'url': wi.url, 'total': formatMillis(wi.daytotal(day, username))});
      }
      total = daySummary.sum;
    }

    var workday =
        !holydays.contains(day) && day.weekday != DateTime.sunday && day.weekday != DateTime.saturday;

    results.add({
      'desc': DATE_FORMAT.format(day),
      'total': formatMillis(total),
      'workday': workday,
      'absences': [],
      'workitems': workitems,
      'future': daySummary != null && day.isAfter(baseDate)
    });
    day = day.add(oneday);
  }
  return results;
}

void logError(ex, StackTrace st) {
  logger.severe("", ex, st);
}

Response handleError(ex, StackTrace st, Request request) {
  logError(ex, st);
  return Response.serverError(body: "${ex}");
}

Future<Map> lastweeks(username, password, page) {
  var service = new AlmReport(username, password);

  return service.login().then((service) {
    var baseDate = new Date.now();
    const MIN_DAYS_BACK = 21;

    if (page != 0) {
      // também tira 2 dias pra mover pro sábado da semana passada
      baseDate = baseDate
          .subtract(new Duration(days: baseDate.weekday - DateTime.monday + 2 + (page * MIN_DAYS_BACK)));
    }
    int daysback = MIN_DAYS_BACK - 7 + baseDate.weekday;
    if (daysback <= MIN_DAYS_BACK) {
      daysback += 7;
    }

    Future buildFuture = service.build(baseDate, daysback).whenComplete(() {
      service.logout().catchError(logError);
    });
    return Future.wait([buildFuture]).then((results) {
      return response({'result': buildReportData(results[0], baseDate, daysback, username)});
    });
  }).catchError((ex) => error(LOGIN_ERROR), test: (e) => e is LoginException);
}

String getRequestIp(Request request) {
  var ip = request.raw.headers.value("X-Forwarded-For");
  if (ip != null) {
    return ip;
  }
  return request.raw.connectionInfo.remoteAddress.address;
}

void printLog(LogRecord rec) {
  var desc = [];
  if (rec.message != null && rec.message != "") {
    desc.add(rec.message);
  }
  if (rec.error != null) {
    desc.add(rec.error.toString());
  }
  var timedesc = "${rec.time}".substring(5);
  print('${rec.level.name}: ${timedesc}: ${rec.loggerName}: ${desc.join(" : ")}');
  if (rec.stackTrace != null) {
    print(rec.stackTrace.toString());
  }
}

class App extends ApplicationChannel {
  @override
  Controller get entryPoint {
    return Router()..route('/rest/lastweeks').link(() => LastWeeksController());
  }
}

class LastWeeksController extends ResourceController {
  @override
  FutureOr<RequestOrResponse> handle(Request request) async {
    try {
      return await super.handle(request);
    } catch (e, s) {
      return handleError(e, s, request);
    }
  }

  @Operation.post()
  Future<Response> getLastWeeks(@Bind.query('user') String username, @Bind.query('passwowrd') String password,
      @Bind.query('page') String paramPage) async {
    logger.info("lastweeks - ${getRequestIp(request)}");
    var page = int.parse(paramPage != null ? paramPage : "0");

    return Response.ok(await lastweeks(username, password, page))..contentType = ContentType.json;
  }
}

void main(List<String> args) {
  var rootpath = new File.fromUri(Platform.script).parent.parent.path;

  Logger.root.onRecord.listen(printLog);

  /**
   * Comandos usados para gerar a base de certificados:
   * export NSS_DEFAULT_DB_TYPE=sql
   * certutil -N -d cert/
   * certutil -A -n icpv2 -t "TCu,Cu,Tuw" -a -i /tmp/icpv2.cert -d cert/
   */
  //SecureSocket.initialize(database: rootpath + Platform.pathSeparator + 'cert', password: "alm123456");

  // algumas chamadas aqui retornam futures, mas como são executadas relativamente rápido e na inicialização do servidor, não se aguarda por elas.
  Intl.defaultLocale = "pt_BR";
  initializeDateFormatting(Intl.defaultLocale, null);

  int port = 9090;
  if (args.isNotEmpty) {
    port = int.parse(args[0]);
  }

  loadConfig(rootpath + Platform.pathSeparator + "config.yaml").then((values) {
    getHolydays().then((it) {
      logger.info("Holydays loaded");
      holydays = new Set.from(it);
    }).catchError(logError);
  });

  Application<App>()
    ..options.address = InternetAddress.loopbackIPv4
    ..options.port = port
    ..start(numberOfInstances: 3).then((server) {
      logger.info('Server started');
    });
}
