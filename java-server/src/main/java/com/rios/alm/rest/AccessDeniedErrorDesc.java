package com.rios.alm.rest;

public class AccessDeniedErrorDesc extends ErrorDesc {

    public AccessDeniedErrorDesc() {
        super("Desculpe, acesso restrito à DECTA.");
    }
}
