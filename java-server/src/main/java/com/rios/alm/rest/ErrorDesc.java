package com.rios.alm.rest;

public class ErrorDesc {

    private String error;

    public ErrorDesc(String error) {
        super();
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
