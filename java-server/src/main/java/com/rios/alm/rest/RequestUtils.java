package com.rios.alm.rest;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rios.alm.util.JsonUtils;
import com.rios.alm.ldap.Ldap;
import com.rios.alm.ldap.LdapUser;
import com.rios.alm.exception.SystemException;

public final class RequestUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestUtils.class);

    private static final boolean USER_VALIDATION_ENABLED = false;

    private RequestUtils() {
    }

    public static void sendJson(HttpServletResponse resp, Object data) {
        try {
            byte[] bytes = JsonUtils.toJson(data).getBytes(StandardCharsets.UTF_8);
            resp.setContentLength(bytes.length);
            resp.setCharacterEncoding("UTF-8");
            resp.setContentType("application/json; charset=UTF-8");
            resp.getOutputStream().write(bytes);
        } catch (IOException e) {
            throw new SystemException(e);
        }
    }

    public static void handleError(Exception ex, HttpServletResponse response) {
        LOGGER.warn("", ex);
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        try {
            response.getWriter().write(ex.getMessage());
        } catch (IOException e) {
            LOGGER.warn("", e);
        }
    }

    public static String getRequestIp(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");
        if (ip != null) {
            return ip;
        }
        return request.getRemoteAddr();
    }

    public static boolean isUserAllowed(String username) {
        if (USER_VALIDATION_ENABLED) {
            LdapUser user = Ldap.getUser(username);
            return user == null || user.isOnDECTA();
        }
        return true;
    }
}
