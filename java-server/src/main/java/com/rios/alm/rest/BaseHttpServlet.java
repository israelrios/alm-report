package com.rios.alm.rest;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class BaseHttpServlet extends HttpServlet {

    private static final long serialVersionUID = 8398739256494617695L;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        try {
            onPost(req, resp);
        } catch (Exception e) {
            RequestUtils.handleError(e, resp);
        }
    }

    protected void onPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
            IOException {
        super.doPost(req, resp);
    }

}
