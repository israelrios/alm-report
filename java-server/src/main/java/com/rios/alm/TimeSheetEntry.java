package com.rios.alm;

import java.time.LocalDate;

import org.apache.commons.lang3.StringUtils;

public class TimeSheetEntry {

    private String username;
    private LocalDate date;
    private int spent;

    @SuppressWarnings("unused")
    private TimeSheetEntry() {
    }

    public TimeSheetEntry(String username, LocalDate date, int spent) {
        super();
        this.username = username;
        this.date = date;
        this.spent = spent;
    }

    public static LocalDate parseDate(String text) {
        String[] parts = StringUtils.split(text.substring(0, 10), '-');
        return LocalDate.of(Integer.valueOf(parts[0]),
            Integer.valueOf(parts[1]),
            Integer.valueOf(parts[2]));
    }

    public String getUsername() {
        return username;
    }

    public LocalDate getDate() {
        return date;
    }

    public int getSpent() {
        return spent;
    }
}
