package com.rios.alm.config;

import java.nio.file.Path;
import java.nio.file.Paths;

import com.rios.alm.util.JsonUtils;

public class Config {

    private static final Config INSTANCE = init();

    private LdapConfig ldap;

    private DbConfig db;

    private Config() {
    }

    private static Config init() {
        Path path = Paths.get("config.json");
        return JsonUtils.fromJson(path.toFile(), Config.class);
    }

    public static Config getInstance() {
        return INSTANCE;
    }

    public LdapConfig getLdap() {
        return ldap;
    }

    public void setLdap(LdapConfig ldap) {
        this.ldap = ldap;
    }

    public DbConfig getDb() {
        return db;
    }

    public void setDb(DbConfig db) {
        this.db = db;
    }
}
