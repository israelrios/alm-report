package com.rios.alm.ldap;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.novell.ldap.LDAPAttribute;
import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPConstraints;
import com.novell.ldap.LDAPEntry;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPSearchResults;
import com.rios.alm.config.Config;
import com.rios.alm.config.LdapConfig;

public final class Ldap {

    private static final Logger LOG = LoggerFactory.getLogger(Ldap.class);

    private static final String LDAP_CONF = "/etc/ldap.conf";
    private static Map<String, LdapUser> cacheUser = Collections.synchronizedMap(new HashMap<>());

    private static volatile LdapConfig systemConfig;

    private Ldap() {
    }

    private static String getAttribute(LDAPEntry entry, String name) {
        LDAPAttribute attribute = entry.getAttribute(name);
        if (attribute != null) {
            String value = attribute.getStringValue();
            if (StringUtils.isNotBlank(value)) {
                return value.trim();
            }
        }
        return null;
    }

    public static LdapUser getUser(String uid) {
        if (cacheUser.containsKey(uid)) {
            return cacheUser.get(uid);
        }

        LdapConfig ldapConfig = getLdapConfig();

        LDAPConnection ldapcon = new LDAPConnection();
        try {
            ldapcon.connect(ldapConfig.getHost(), ldapConfig.getPort());

            LDAPConstraints constraints = ldapcon.getConstraints();
            constraints.setTimeLimit(200);
            ldapcon.bind(
                LDAPConnection.LDAP_V3,
                ldapConfig.getBindDN(),
                ldapConfig.getPassword().getBytes(StandardCharsets.UTF_8),
                constraints);

            LDAPSearchResults results = ldapcon.search(
                "dc=serpro,dc=gov,dc=br",
                LDAPConnection.SCOPE_SUB,
                String.format("uid=%s", uid),
                new String[] { "employeeNumber", "cn", "usrlocal", "ou" },
                false);

            if (results.hasMore()) {
                LDAPEntry entry = results.next();
                String value = getAttribute(entry, "employeeNumber");
                Long matricula = null;
                if (StringUtils.isNotBlank(value)) {
                    matricula = Long.valueOf(value.trim());
                }
                String name = getAttribute(entry, "cn");
                String userLocal = getAttribute(entry, "usrlocal");
                String organizationalUnit = getAttribute(entry, "ou");
                LdapUser user = new LdapUser(matricula, name, userLocal, organizationalUnit);
                cacheUser.put(uid, user);
                return user;
            }
        } catch (Exception e) {
            LOG.warn("", e);
        } finally {
            // sempre fecha a conexão porque não sei qual é o comportamento do ldap se ficar conectado
            close(ldapcon);
        }
        return null;
    }

    private static LdapConfig getLdapConfig() {
        LdapConfig ldapConfig = null;
        try {
            ldapConfig = readSystemConfig();
        } catch (Exception e) {
            LOG.warn("Couldn't read " + LDAP_CONF, e);
        }
        if (ldapConfig == null) {
            ldapConfig = Config.getInstance().getLdap();
        }
        return ldapConfig;
    }

    private static LdapConfig readSystemConfig() throws IOException {
        if (systemConfig != null) {
            return systemConfig;
        }

        File ldapConfFile = new File(LDAP_CONF);
        if (!ldapConfFile.exists()) {
            return null;
        }

        LOG.info("Reading {}", LDAP_CONF);

        Pattern patValues = Pattern.compile("(host|bindpw|binddn|port|uri)\\s+(.*)");

        try (BufferedReader reader = new BufferedReader(new FileReader(ldapConfFile))) {

            LdapConfig config = new LdapConfig();
            config.setPort(389);

            String line;

            while ((line = reader.readLine()) != null) {
                Matcher mo = patValues.matcher(line);
                if (mo.matches()) {
                    String name = mo.group(1);
                    String value = mo.group(2);
                    switch(name) {
                    case "uri":
                        value = value.replaceAll("ldap[si]?://", "");
                        config.setHost(value);
                        break;
                    case "host":
                        // the method LDAPConnection.connect accepts multiple hosts, so nothing to do here.
                        config.setHost(value);
                        break;
                    case "bindpw":
                        config.setPassword(value.trim());
                        break;
                    case "binddn":
                        config.setBindDN(value.trim());
                        break;
                    case "port":
                        config.setPort(Integer.parseInt(value.trim()));
                        break;
                    default:
                        // ignora
                    }
                }
            }
            systemConfig = config;
            return config;
        }
    }

    private static void close(LDAPConnection ldapcon) {
        try {
            if (ldapcon != null) {
                ldapcon.disconnect();
            }
        } catch (LDAPException ex) {
            // ignora
        }
    }
}
