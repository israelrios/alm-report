package com.rios.alm.ldap;

import org.apache.commons.lang3.StringUtils;

public class LdapUser {

    private Long matricula;
    private String name;
    private String userLocal;
    private String organizationalUnit;

    LdapUser(Long matricula, String name, String userLocal, String organizationalUnit) {
        super();
        this.name = name;
        this.matricula = matricula;
        this.userLocal = userLocal;
        this.organizationalUnit = organizationalUnit;
    }

    public Long getMatricula() {
        return matricula;
    }

    public String getName() {
        return name;
    }

    public String getUserLocal() {
        return userLocal;
    }

    public String getOrganizationalUnit() {
        return organizationalUnit;
    }

    public boolean isOnDECTA() {
        return (StringUtils.isNotBlank(userLocal) && userLocal.contains("DECTA"))
                || (StringUtils.isNotBlank(organizationalUnit) && organizationalUnit.contains("DECTA"));
    }
}
