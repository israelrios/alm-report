package com.rios.alm;

public class Absence {

    private String description;
    private int hours;

    public Absence(String description, int hours) {
        super();
        this.description = description;
        this.hours = hours;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }
}
