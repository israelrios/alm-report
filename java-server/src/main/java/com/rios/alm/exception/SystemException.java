package com.rios.alm.exception;

public class SystemException extends RuntimeException {

    private static final long serialVersionUID = 7788821741347276012L;

    public SystemException(String message) {
        super(message);
    }

    public SystemException(Throwable cause) {
        super(cause);
    }

    public SystemException(String message, Throwable cause) {
        super(message, cause);
    }
}
