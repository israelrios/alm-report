package com.rios.alm.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdScalarSerializer;

public final class JsonUtils {

    private static final SimpleModule MODULE = new SimpleModule();

    static {
        MODULE.addDeserializer(LocalDate.class, new LocalDateDeserializer());
        MODULE.addSerializer(new LocalDateSerializer());
    }

    private JsonUtils() {
    }

    /**
     * Converte o objeto para o formato JSON e retorna uma string contendo o conteúdo JSON .
     */
    public static String toJson(Object object) {
        return toJson(object, false);
    }

    /**
     * Converte o objeto para o formato JSON e retorna uma string contendo o conteúdo JSON .
     */
    public static String toJson(Object object, boolean useFields) {
        ObjectMapper mapper = createMapper(useFields);
        try {
            return mapper.writeValueAsString(object);
        } catch (Exception e) {
            throw new JsonUtilsException(e);
        }
    }

    private static ObjectMapper createMapper(boolean useFields) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"));
        mapper.registerModule(MODULE);
        if (useFields) {
            mapper.setVisibilityChecker(mapper.getSerializationConfig().getDefaultVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
        }
        return mapper;
    }

    /**
     * Converte o objeto para o formato JSON e escreve o conteúdo JSON no arquivo informado.
     */
    public static void toJson(Object obj, File file) {
        toJson(obj, file, false);
    }

    /**
     * Converte o objeto para o formato JSON e escreve o conteúdo JSON no arquivo informado.
     */
    public static void toJson(Object obj, File file, boolean useFields) {
        ObjectMapper mapper = createMapper(useFields);
        try {
            mapper.writeValue(file, obj);
        } catch (IOException e) {
            throw new JsonUtilsException(e);
        }
    }

    /**
     * Cria e retorna um objeto a partir do conteúdo do arquivo cujo conteúdo está no padrão JSON.
     */
    public static <T> T fromJson(File file, Class<T> classe) {
        return fromJson(file, classe, false);
    }

    /**
     * Cria e retorna um objeto a partir do conteúdo do arquivo cujo conteúdo está no padrão JSON.
     */
    public static <T> T fromJson(File file, Class<T> classe, boolean useFields) {
        ObjectMapper mapper = createMapper(useFields);
        try {
            return mapper.readValue(file, classe);
        } catch (IOException e) {
            throw new JsonUtilsException(e);
        }
    }

    /**
     * Cria e retorna um objeto a partir do JSON.
     */
    public static <T> T fromJson(String json, Class<T> classe) {
        return fromJson(json, classe, false);
    }

    /**
     * Cria e retorna um objeto a partir do JSON.
     */
    public static <T> T fromJson(String json, Class<T> classe, boolean useFields) {
        ObjectMapper mapper = createMapper(useFields);
        try {
            return mapper.readValue(json, classe);
        } catch (IOException e) {
            throw new JsonUtilsException(e);
        }
    }

    private static class LocalDateDeserializer extends StdScalarDeserializer<LocalDate> {

        private static final long serialVersionUID = -729958768295405109L;

        protected LocalDateDeserializer() {
            super(LocalDate.class);
        }

        @Override
        public LocalDate deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
            String text = jp.readValueAs(String.class);
            return LocalDate.parse(text);
        }

    }

    private static class LocalDateSerializer extends StdScalarSerializer<LocalDate> {

        protected LocalDateSerializer() {
            super(LocalDate.class);
        }

        @Override
        public void serialize(LocalDate value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException {
            jgen.writeString(value.format(DateTimeFormatter.ISO_LOCAL_DATE));
        }
    }

    public static class JsonUtilsException extends RuntimeException {
        private static final long serialVersionUID = -2202859621309601162L;

        public JsonUtilsException(Throwable cause) {
            super(cause);
        }
    }
}
