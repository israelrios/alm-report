package com.rios.alm.timecard;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.rios.alm.exception.LoginException;
import com.rios.alm.http.BasicHttpClient;
import com.rios.alm.http.LoggedInClient;
import com.rios.alm.util.JsonUtils;

class SiscopTimeCardSource extends LoggedInClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(SiscopTimeCardSource.class);

    private static final String URL_BASE = "https://siscopweb.serpro";

    private static final String URL_API = URL_BASE + "/api";

    private static final String URL_EMPLOYEE = URL_API + "/empregado/0";

    private static final String URL_QUERY = URL_API + "/manutencao";

    private static final String URL_AUTH = URL_API + "/auth";

    private static final BasicHttpClient CLIENT = makeHttpClient();

    SiscopTimeCardSource(String username, String password) {
        super(CLIENT, username, password);
    }

    private static BasicHttpClient makeHttpClient() {

        List<BasicHeader> defaultHeaders = new ArrayList<>();
        defaultHeaders.add(new BasicHeader("Content-Type", ContentType.APPLICATION_JSON.toString()));

        return new BasicHttpClient(defaultHeaders) {
            @Override
            protected void interceptError(HttpResponse response, HttpContext context) {
                if (response.getStatusLine().getStatusCode() == 401) {
                    logRemoteError(response);
                    throw new LoginException(URL_AUTH);
                }
                super.interceptError(response, context);
            }
        };
    }

    Map<LocalDate, Integer> makeTimeCardMap(LocalDate baseDate) {

        if (session.getMatricula() == null) {
            Map map = fromJson(buildGet(URL_EMPLOYEE), Map.class);
            session.setMatricula((Integer) map.get("matricula"));
        }

        LOGGER.info("Getting time card for {}. Date: {}", username, baseDate);

        String smonth = String.format("%04d%02d", baseDate.getYear(), baseDate.getMonth().getValue());

        RequestBuilder requestBuilder = buildGet(URL_QUERY + "/" + session.getMatricula() + "/" + smonth);

        SiscopMonthReport monthReport = fromJson(requestBuilder, SiscopMonthReport.class);

        Map<LocalDate, Integer> result = new HashMap<>();

        for (SiscopDay day : monthReport.days) {
            LocalDate itemDate = baseDate.withDayOfMonth(day.day);

            int total = sumDayTime(day);

            if (total == 0) {
                continue;
            }
            result.put(itemDate, total);
        }
        return result;
    }

    private int sumDayTime(SiscopDay day) {
        LocalTime startTime = null;
        int total = 0;
        for (SiscopRecord record : day.records) {
            if (StringUtils.isBlank(record.time) || StringUtils.isBlank(record.type)
                || "--:--".equals(record.time)) {
                startTime = null;
                continue;
            }

            LocalTime time = LocalTime.parse(record.time);

            if ("E".equals(record.type)) {
                startTime = time;
            } else if (startTime != null) {
                total += time.get(ChronoField.MILLI_OF_DAY) - startTime.get(ChronoField.MILLI_OF_DAY);
                startTime = null;
            }
        }
        return total;
    }

    @Override
    protected void configRequest(HttpUriRequest request) {
        request.setHeader("Authorization", "Token " + session.getSiscopToken());
    }

    @Override
    protected boolean isLoggedIn() {
        return session.getSiscopToken() != null;
    }

    @Override
    protected void login() {
        if (session.getSiscopToken() != null) {
            LOGGER.info("Login requested.");
        }
        LOGGER.info("Login");

        session.setSiscopToken(null);

        Map<String, String> body = new HashMap<>();
        body.put("username", username);
        body.put("password", password);

        HttpUriRequest request = buildPost(URL_AUTH)
            .setEntity(new StringEntity(JsonUtils.toJson(body), ContentType.APPLICATION_JSON))
            .build();
        CloseableHttpResponse response = executeLoggedOff(request);

        Header[] headers = response.getHeaders("Set-Token");

        EntityUtils.consumeQuietly(response.getEntity());

        session.setSiscopToken(headers[0].getValue());
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
    public static class SiscopMonthReport {
        @JsonProperty("dias")
        List<SiscopDay> days = new ArrayList<>();
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
    public static class SiscopDay {
        @JsonProperty("dia")
        int day;
        @JsonProperty("registros")
        List<SiscopRecord> records = new ArrayList<>();
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
    public static class SiscopRecord {
        @JsonProperty("hora")
        String time;
        @JsonProperty("tipo")
        String type;
    }

}
