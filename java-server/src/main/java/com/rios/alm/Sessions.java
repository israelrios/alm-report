package com.rios.alm;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

import javax.annotation.ParametersAreNonnullByDefault;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

@ParametersAreNonnullByDefault
public final class Sessions {

    private static final LoadingCache<String, Session> SESSION_CACHE = CacheBuilder.newBuilder()
        .maximumSize(500)
        .concurrencyLevel(3)
        .expireAfterAccess(30, TimeUnit.MINUTES)
        .build(new CacheLoader<String, Session>() {
            @Override
            public Session load(String key) {
                return new Session();
            }
        });

    private Sessions() {
    }

    public static Session get(String username, String password) {
        // Associa com a senha para evitar que seja possível acessar a sessão apenas com o cpf.
        HashFunction hashFunction = Hashing.sha256();
        String sessionKey = username + hashFunction.hashString(password, StandardCharsets.UTF_8).toString();

        return SESSION_CACHE.getUnchecked(sessionKey);
    }
}
