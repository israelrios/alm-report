package com.rios.alm;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rios.alm.db.DBPool;
import com.rios.alm.exception.SystemException;
import com.rios.alm.ldap.Ldap;
import com.rios.alm.ldap.LdapUser;

public class EscritorioProjetos {

    private static final EscritorioProjetos instance = new EscritorioProjetos();

    private EscritorioProjetos() {
    }

    public static EscritorioProjetos getInstance() {
        return instance;
    }

    public Map<LocalDate, List<Absence>> getAbsences(String cpf) {
        LdapUser user = Ldap.getUser(cpf);
        HashMap<LocalDate, List<Absence>> result = new HashMap<>();
        if (user == null || user.getMatricula() == null) {
            return result;
        }

        final String querySql = "select dia, descricao, horas from ausencias where matricula = ?";

        try (Connection con = DBPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(querySql)) {

            ps.setLong(1, user.getMatricula());
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    LocalDate day = rs.getTimestamp(1).toLocalDateTime().toLocalDate();
                    String desc = rs.getString(2);
                    int hours = rs.getInt(3);
                    Absence absence = new Absence(desc, hours);
                    List<Absence> lst = result.computeIfAbsent(day, k -> new ArrayList<>());
                    lst.add(absence);
                }
            }
        } catch (SQLException e) {
            throw new SystemException(e);
        }
        return result;
    }
}
