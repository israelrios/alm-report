package com.rios.alm;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.Futures;
import com.rios.alm.db.JsonDB;
import com.rios.alm.exception.RemoteServiceException;
import com.rios.alm.exception.SystemException;

public class AlmReport {

    private static final Logger LOGGER = LoggerFactory.getLogger(AlmReport.class);

    private static final int TIMEOUT = 15000; // 15s

    private final String username;

    private final JsonDB<WorkItem> workItemDb;

    private final AlmService service;

    // chave é o cpf
    private final JsonDB<String> lastWorkItemUserDb;

    public AlmReport(String username, String password) {
        this.username = username;

        service = new AlmService(username, password);

        workItemDb = new JsonDB<>("wi:", WorkItem.class);
        lastWorkItemUserDb = new JsonDB<>("lwiu:", String.class);
    }

    public Collection<Project> getWorkItemsOpenOrInProgress() {

        List<Project> projects = service.getProjects();
        List<Future<Void>> futures = new ArrayList<>(projects.size());
        for (Project project : projects) {
            FutureTask<Void> futureTask = new FutureTask<>(() -> {
                service.loadWorkItemsOpenOrInProgress(project);
                return null;
            });
            futures.add(futureTask);
            Main.getThreadPool().execute(futureTask);
        }
        for (Future<Void> future : futures) {
            try {
                future.get(TIMEOUT, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new SystemException(e);
            } catch (ExecutionException | TimeoutException e) {
                throw new RemoteServiceException(e);
            }
        }
        return projects;
    }

    private Iterable<WorkItem> loadWorkItems(LocalDate date, int daysback) {
        LOGGER.info("Loading workitems - {}", username);

        Map<String, String> wiupdated = getUpdatedWorkItems(date, daysback);

        // carrega o id do workitem com maior data de alteração presente na última atualização.
        String lastWorkItemId = lastWorkItemUserDb.load(username);

        List<WorkItem> workitems = new ArrayList<>(wiupdated.size());
        List<Future<WorkItem>> futures = new ArrayList<>(wiupdated.size());

        String newLastWorkItemId = null;
        String maxDate = "";

        for (Entry<String, String> e : wiupdated.entrySet()) {
            String updated = e.getValue();
            String wid = e.getKey();
            Future<WorkItem> future = loadWorkItem(wid, updated, lastWorkItemId);
            futures.add(future);

            if (updated.compareTo(maxDate) > 0) {
                maxDate = updated;
                newLastWorkItemId = wid;
            }
        }

        for (Future<WorkItem> future : futures) {
            try {
                workitems.add(future.get(TIMEOUT, TimeUnit.MILLISECONDS));
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new SystemException(e);
            } catch (ExecutionException | TimeoutException e) {
                throw new RemoteServiceException(e);
            }
        }

        lastWorkItemUserDb.save(username, newLastWorkItemId);
        return workitems;
    }

    private Map<String, String> getUpdatedWorkItems(LocalDate date, int daysback) {
        /*
         * Retorna os work itens do usuário modificados recentemente. Adiciona 7 dias pra pegar as apropiações
         * realizadas no futuro.
         */
        LocalDateTime startDate = date.minusDays(daysback + 7L).atStartOfDay();

        List<ChangeFeedEntry> changes = service.getChanges(startDate);

        Map<String, String> wiupdated = new HashMap<>();

        for (ChangeFeedEntry change : changes) {

            if (!isTimeSheetChange(change)) {
                continue;
            }
            addWorkItemChange(wiupdated, change);
        }
        return wiupdated;
    }

    private void addWorkItemChange(Map<String, String> wiupdated, ChangeFeedEntry change) {
        String url = change.getUrl();
        String[] urlparts = url.split("/");
        String wid = urlparts[urlparts.length - 1];
        // O id também pode ser obtido assim:
        // wid = XmlUtils.first("syndication:itemId", entry).getTextContent()

        // assegura que o id do workitem foi pego corretamente. Deve ser um inteiro.
        if (!NumberUtils.isDigits(wid)) {
            throw new NumberFormatException("Workitem id is invalid: " + wid);
        }

        // este feed traz todas as alterações em um item, podendo o mesmo item constar mais de um vez na
        // listagem
        if (wiupdated.containsKey(wid)) {
            return;
        }

        wiupdated.put(wid, change.getUpdated());
    }

    private WorkItem getFromDbOrCreate(String wid) {
        WorkItem workItem = workItemDb.load(wid);
        if (workItem == null) {
            workItem = new WorkItem(wid, service.getWorkItemUrl(wid));
        }
        return workItem;
    }

    private Future<WorkItem> loadWorkItem(String wid, String modified, String lastWorkItemId) {
        WorkItem workItem = getFromDbOrCreate(wid);
        // o último workitem carregado na atualização anterior é sempre recarregado para contornar a
        // compactação do feed pelo alm.
        if (workItem.isUpToDate(modified) && !wid.equals(lastWorkItemId)) {
            return Futures.immediateFuture(workItem);
        }
        FutureTask<WorkItem> future = new FutureTask<>(() -> loadRemoteWorkItem(wid, workItem));
        Main.getThreadPool().execute(future);
        return future;
    }

    private WorkItem loadRemoteWorkItem(String wid, WorkItem workItem) {
        service.loadWorkItem(wid, workItem);
        workItemDb.save(wid, workItem);
        return workItem;
    }

    /**
     * Verifica se a entrada é de modificação da time sheet.
     * 
     * @param change
     *            Entrada do feed.
     * @return True se é uma entrada de modificação da time seet.
     */
    private boolean isTimeSheetChange(ChangeFeedEntry change) {
        final String summary = change.getSummary().toLowerCase();
        if (summary.contains("time sheet") || summary.contains("tempo gasto")
            || summary.contains("time spent")) {
            return true;
        }

        return change.getRefTypes() != null && change.getRefTypes().toLowerCase().contains("time sheet");
    }

    private Map<LocalDate, DaySummary> buildDayMap(Iterable<WorkItem> workitems, LocalDate date,
        int daysback) {
        Map<LocalDate, DaySummary> daymap = new HashMap<>();
        for (WorkItem wi : workitems) {
            for (TimeSheetEntry tse : wi.getTimesheet()) {
                // considera somente os últimos daysback dias
                if (Period.between(date, tse.getDate()).getDays() > daysback
                    || !tse.getUsername().equals(username) || tse.getSpent() == 0) {
                    continue;
                }
                DaySummary day = daymap.get(tse.getDate());
                if (day == null) {
                    day = new DaySummary();
                    daymap.put(tse.getDate(), day);
                }
                day.sum(tse.getSpent());
                day.getWorkitems().add(wi);
            }
        }
        return daymap;
    }

    public Map<LocalDate, DaySummary> build(LocalDate date, int daysback) {
        Iterable<WorkItem> workItems = loadWorkItems(date, daysback);
        return buildDayMap(workItems, date, daysback);
    }

}
