package com.rios.alm;

import java.util.Set;
import java.util.TreeSet;

import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public class Project implements Comparable<Project> {

    private final String id;

    private final String name;

    private Set<WorkItem> workItems = new TreeSet<>();

    private WorkItem nullParent;

    public Project(String id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<WorkItem> getWorkItems() {
        return workItems;
    }

    @Override
    public int compareTo(Project o) {
        return name.compareTo(o.getName());
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Project)) {
            return false;
        }
        return id.equals(((Project) obj).getId());
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    protected WorkItem getNullParent() {
        if (nullParent == null) {
            nullParent = new WorkItem("X", null);
        }
        return nullParent;
    }
}
