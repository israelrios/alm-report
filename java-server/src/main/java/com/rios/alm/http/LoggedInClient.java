package com.rios.alm.http;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.BasicCookieStore;

import com.rios.alm.Session;
import com.rios.alm.Sessions;
import com.rios.alm.exception.LoginException;

public abstract class LoggedInClient extends StatefullClient {

    protected final Session session;
    protected final String username;
    protected final String password;

    public LoggedInClient(BasicHttpClient client, String username, String password) {
        super(client);
        this.username = username;
        this.password = password;
        session = Sessions.get(username, password);
    }

    protected abstract void login();

    protected abstract boolean isLoggedIn();

    protected CloseableHttpResponse executeLoggedOff(HttpUriRequest request) {
        return super.execute(request);
    }

    @Override
    protected CloseableHttpResponse execute(HttpUriRequest request) {
        if (!isLoggedIn()) {
            login();
        }
        try {
            configRequest(request);
            return super.execute(request);
        } catch (LoginException e) {
            login();
            return super.execute(request);
        }
    }

    protected void configRequest(HttpUriRequest request) {
    }

    @Override
    protected BasicCookieStore getCookieStore() {
        return session.getCookieStore();
    }
}
