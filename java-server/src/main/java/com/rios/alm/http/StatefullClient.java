package com.rios.alm.http;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.util.EntityUtils;

import com.rios.alm.exception.RemoteServiceException;
import com.rios.alm.util.JsonUtils;

public class StatefullClient {

    // HttpClientContext não é thread safe. Assim, é necessário usar um ThreadLocal porque usamos várias
    // threads para carregar tarefas mais rapidamente.
    // Não deve ser estático porque deve estar vinculado ao usuário.
    private final ThreadLocal<HttpClientContext> context = new ThreadLocal<>();

    private final BasicHttpClient client;

    public StatefullClient(BasicHttpClient client) {
        this.client = client;
    }

    protected BasicCookieStore getCookieStore() {
        return null;
    }

    protected HttpClientContext getContext() {
        HttpClientContext ctx = context.get();
        if (ctx != null) {
            return ctx;
        }
        ctx = HttpClientContext.create();

        BasicCookieStore cookieStore = getCookieStore();
        if (cookieStore != null) {
            ctx.setCookieStore(cookieStore);
        }

        context.set(ctx);
        return ctx;
    }

    protected void disableResponseInterceptor() {
        getContext().setAttribute(BasicHttpClient.DISABLE_RESPONSE_INTERCEPTOR_ATTR, true);
    }

    protected void enableResponseInterceptor() {
        getContext().removeAttribute(BasicHttpClient.DISABLE_RESPONSE_INTERCEPTOR_ATTR);
    }

    protected RequestBuilder buildPost(String url) {
        return RequestBuilder.post().setUri(url);
    }

    protected RequestBuilder buildGet(String url) {
        return RequestBuilder.get().setUri(url);
    }

    protected CloseableHttpResponse execute(HttpUriRequest request) {
        return client.execute(request, getContext());
    }

    protected String getText(HttpUriRequest request) {
        return getText(execute(request));
    }

    protected String getText(RequestBuilder requestBuilder) {
        return getText(requestBuilder.build());
    }

    protected static String getText(HttpResponse response) {
        try {
            return EntityUtils.toString(response.getEntity(), "UTF-8");
        } catch (ParseException | IOException e) {
            throw new RemoteServiceException(e);
        }
    }

    protected <T> T fromJson(RequestBuilder requestBuilder, Class<T> cl) {
        return JsonUtils.fromJson(getText(execute(requestBuilder.build())), cl);
    }
}
