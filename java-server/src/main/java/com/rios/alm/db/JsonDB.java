package com.rios.alm.db;

import com.rios.alm.util.JsonUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class JsonDB<T> {

    private static final JedisPool pool = new JedisPool(new JedisPoolConfig(), "localhost");

    private String prefix;
    private Class<T> itemClass;

    public JsonDB(String prefix, Class<T> itemClass) {
        this.prefix = prefix;
        this.itemClass = itemClass;
    }

    public T load(String key) {
        String json;
        try (Jedis db = pool.getResource()) {
            json = db.get(prefix + key);
        }
        if (json != null) {
            return JsonUtils.fromJson(json, itemClass, true);
        }
        return null;
    }

    public void save(String key, T value) {
        if (value == null) {
            delete(key);
            return;
        }
        String json = JsonUtils.toJson(value, true);
        try (Jedis db = pool.getResource()) {
            db.set(prefix + key, json);
        }
    }

    public void delete(String key) {
        try (Jedis db = pool.getResource()) {
            db.del(prefix + key);
        }
    }
}
