package com.rios.alm.db;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbcp2.ConnectionFactory;
import org.apache.commons.dbcp2.DriverManagerConnectionFactory;
import org.apache.commons.dbcp2.PoolableConnection;
import org.apache.commons.dbcp2.PoolableConnectionFactory;
import org.apache.commons.dbcp2.PoolingDataSource;
import org.apache.commons.pool2.impl.GenericObjectPool;

import com.rios.alm.config.Config;
import com.rios.alm.config.DbConfig;
import com.rios.alm.exception.SystemException;

public class DBPool {

    private static final DBPool instance = new DBPool();

    private PoolingDataSource<PoolableConnection> dataSource;

    private DBPool() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new DBPoolConfigurationException(e);
        }
        DbConfig dbConfig = Config.getInstance().getDb();
        ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(dbConfig.getUrl(),
            dbConfig.getUsername(),
            dbConfig.getPassword());
        PoolableConnectionFactory poolableConnectionFactory = new PoolableConnectionFactory(connectionFactory,
            null);
        poolableConnectionFactory.setValidationQuery("select 1");
        poolableConnectionFactory.setPoolStatements(true);
        poolableConnectionFactory.setValidationQueryTimeout(2000); // 2s
        GenericObjectPool<PoolableConnection> connectionPool =
            new GenericObjectPool<>(poolableConnectionFactory);
        connectionPool.setMaxIdle(5);
        connectionPool.setMaxTotal(20);
        connectionPool.setTestWhileIdle(true);
        connectionPool.setTimeBetweenEvictionRunsMillis(60 * 1000L); // 1min
        poolableConnectionFactory.setPool(connectionPool);
        dataSource = new PoolingDataSource<>(connectionPool);
    }

    public Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new SystemException(e);
        }
    }

    public static DBPool getInstance() {
        return instance;
    }

    public static class DBPoolConfigurationException extends RuntimeException {

        private static final long serialVersionUID = -294328216244608881L;

        public DBPoolConfigurationException(Throwable cause) {
            super(cause);
        }
    }
}
