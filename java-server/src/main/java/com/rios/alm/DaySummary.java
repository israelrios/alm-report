package com.rios.alm;

import java.util.ArrayList;
import java.util.List;

public class DaySummary {

    private int sum = 0;
    private List<WorkItem> workitems = new ArrayList<>();

    public int getSum() {
        return sum;
    }

    public void sum(int sum) {
        this.sum += sum;
    }

    public List<WorkItem> getWorkitems() {
        return workitems;
    }
}
