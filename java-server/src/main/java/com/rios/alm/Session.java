package com.rios.alm;

import java.time.LocalDateTime;

import org.apache.http.impl.client.BasicCookieStore;

public class Session {

    private LocalDateTime loginAlm;
    private LocalDateTime loginBiCorp;
    private String siscopToken;
    private Integer matricula;
    private BasicCookieStore cookieStore = new BasicCookieStore();

    public String getSiscopToken() {
        return siscopToken;
    }

    public void setSiscopToken(String siscopToken) {
        this.siscopToken = siscopToken;
    }

    public Integer getMatricula() {
        return matricula;
    }

    public void setMatricula(Integer matricula) {
        this.matricula = matricula;
    }

    public BasicCookieStore getCookieStore() {
        return cookieStore;
    }

    public LocalDateTime getLoginAlm() {
        return loginAlm;
    }

    public void setLoginAlm(LocalDateTime loginAlm) {
        this.loginAlm = loginAlm;
    }

    public LocalDateTime getLoginBiCorp() {
        return loginBiCorp;
    }

    public void setLoginBiCorp(LocalDateTime loginBiCorp) {
        this.loginBiCorp = loginBiCorp;
    }
}
