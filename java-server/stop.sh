#!/bin/bash

pid=$(ps aux | grep alm-report.jar | grep -v grep | awk -F" " '{print $2}')

if [ -n "$pid" ] ; then
    echo "Killing $pid"
    kill $pid
    sleep 0.3
    while ps -p $pid > /dev/null; do
	sleep 1
    done
fi
