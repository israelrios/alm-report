#!/bin/bash

cd "$( dirname "${BASH_SOURCE[0]}" )"

./stop.sh

export JAVA_HOME=/opt/jdk11

${JAVA_HOME}/bin/java -server -Xmx32m -Xms32m -jar alm-report.jar >> /tmp/alm-report.log 2>&1 &
